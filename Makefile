
CC ?= gcc
AR ?= ar
CFLAGS ?= -g -O2
CPPFLAGS = -I. -D_GNU_SOURCE

prefix:=/usr
bindir=$(prefix)/bin
libdir=$(prefix)/lib
incdir=$(prefix)/include
docdir=$(prefix)/share/doc/crypt
DLLVER=0

STATICLIB=libcrypt.a
SHAREDLIB=cygcrypt-$(DLLVER).dll
IMPORTLIB=libcrypt.dll.a

LIBS=$(STATICLIB) $(SHAREDLIB)

SRCS=crypt.c crypt_blowfish.c crypt_des.c crypt_md5.c crypt_r.c \
     crypt_sha256.c crypt_sha512.c encrypt.c
OBJS=$(SRCS:.c=.o)

all: $(LIBS)

$(STATICLIB): $(OBJS)
	$(AR) rv $@ $^

$(SHAREDLIB): $(OBJS) crypt.def
	$(CC) -shared -Wl,--gc-sections -Wl,--out-implib=$(IMPORTLIB) $^ -o $@

crypt.o: crypt.h

crypt_r.o: crypt.h

distclean: clean

clean:
	-rm *.o *.a *.dll

install: all
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(libdir)
	install -d $(DESTDIR)$(incdir)
	install -d $(DESTDIR)$(docdir)
	install -m 644 crypt.h         $(DESTDIR)$(incdir)/crypt.h
	install -m 644 $(STATICLIB)    $(DESTDIR)$(libdir)
	install -m 644 $(IMPORTLIB)    $(DESTDIR)$(libdir)
	install -m 755 $(SHAREDLIB)    $(DESTDIR)$(bindir)
	install -m 644 README          $(DESTDIR)$(docdir)

